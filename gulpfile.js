import "./config/global.js";

import html from "./config/tasks/html.js";
import clean from "./config/tasks/clean.js";
import server from "./config/tasks/server.js";
import watch from "./config/tasks/watch.js";
import scss from "./config/tasks/scss.js";
import scripts from "./config/tasks/scripts.js";
import fonts from "./config/tasks/fonts.js";
import images from "./config/tasks/images.js";
import favicons from "./config/tasks/favicons.js";

const tasks = [clean, html, scss, scripts, fonts, images, favicons];
export default $.gulp.series(...tasks, $.gulp.parallel(watch, server));

//
// const prod = gulp.series(
//   "clean",
//   gulp.parallel(["pages", "styles", "scripts", "images", "fonts", "favicons"])
// );
//
