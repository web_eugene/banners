import { paths } from "./paths.js";
import gulp from "gulp";
import { deleteAsync } from "del";
import fileInclude from "gulp-file-include";
import BrowserSync from "browser-sync";
import plumber from "gulp-plumber";
import notify from "gulp-notify";

global.$ = {
  paths,
  gulp,
  plumber,
  notify,
  deleteAsync,
  fileInclude,
  BrowserSync: BrowserSync.create(),
};
