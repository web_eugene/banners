const fonts = () => {
  return $.gulp
    .src($.paths.fonts.src)
    .pipe(
      $.plumber({
        errorHandler: $.notify.onError((error) => ({
          title: "Oh no! Error in Fonts",
          message: error.message,
        })),
      })
    )
    .pipe($.gulp.dest($.paths.fonts.public))
    .pipe($.BrowserSync.stream());
};

export default fonts;
