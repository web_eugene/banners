const server = () => {
  $.BrowserSync.init({
    server: "./public/",
    port: 5000,
  });
};

export default server;
