import gulpBabel from "gulp-babel";

const scripts = () => {
  return $.gulp
    .src($.paths.scripts.src, { sourcemaps: true })
    .pipe(
      $.plumber({
        errorHandler: $.notify.onError((error) => ({
          title: "Oh no! Error in JS",
          message: error.message,
        })),
      })
    )
    .pipe(
      gulpBabel({
        presets: ["@babel/preset-env"],
      })
    )
    .pipe($.gulp.dest($.paths.scripts.public, { sourcemaps: true }))
    .pipe($.BrowserSync.stream());
};

export default scripts;
