const images = () => {
  return $.gulp
    .src($.paths.images.src)
    .pipe(
      $.plumber({
        errorHandler: $.notify.onError((error) => ({
          title: "Oh no! Error in Images",
          message: error.message,
        })),
      })
    )
    .pipe($.gulp.dest($.paths.images.public))
    .pipe($.BrowserSync.stream());
};

export default images;
