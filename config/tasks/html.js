import fs from "fs";

const html = () => {
  const data = JSON.parse(fs.readFileSync("./src/data/index.json", "utf8"));

  return $.gulp
    .src($.paths.views.src)
    .pipe(
      $.plumber({
        errorHandler: $.notify.onError((error) => ({
          title: "Oh no! Error in HTML",
          message: error.message,
        })),
      })
    )
    .pipe(
      $.fileInclude({
        basepath: "./src/views/",
        context: data,
      })
    )
    .pipe($.gulp.dest($.paths.views.public))
    .pipe($.BrowserSync.stream());
};

export default html;
