import html from "./html.js";
import scripts from "./scripts.js";
import fonts from "./fonts.js";
import scss from "./scss.js";
import images from "./images.js";
import favicons from "./favicons.js";

const watch = () => {
  $.gulp.watch($.paths.views.watch, html);
  $.gulp.watch($.paths.scripts.watch, scripts);
  $.gulp.watch($.paths.scss.watch, scss);
  $.gulp.watch($.paths.images.watch, images);
  $.gulp.watch($.paths.fonts.watch, fonts);
  $.gulp.watch($.paths.favicons.watch, favicons);
};

export default watch;
