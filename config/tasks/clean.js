const clean = () => {
  return $.deleteAsync(["./public/*"]);
};

export default clean;
