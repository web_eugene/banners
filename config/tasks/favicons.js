import favIcons from "gulp-favicons";

const favicons = () => {
  return $.gulp
    .src($.paths.favicons.src)
    .pipe(
      $.plumber({
        errorHandler: $.notify.onError((error) => ({
          title: "Oh no! Error in Favicons",
          message: error.message,
        })),
      })
    )
    .pipe(
      favIcons({
        icons: {
          favicons: true,
          appleIcon: true,
          android: false,
          online: false,
          appleStartup: false,
          firefox: false,
          yandex: false,
          windows: false,
          coast: false,
        },
      })
    )
    .pipe($.gulp.dest($.paths.favicons.public))
    .pipe($.BrowserSync.stream());
};

export default favicons;
