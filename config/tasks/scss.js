import dartSass from "sass";
import gulpSass from "gulp-sass";
import groupMedia from "gulp-group-css-media-queries";
import autoprefixer from "gulp-autoprefixer";

const scss = () => {
  return $.gulp
    .src($.paths.scss.src, { sourcemaps: true })
    .pipe(
      $.plumber({
        errorHandler: $.notify.onError((error) => ({
          title: "Oh no! Error in Scss",
          message: error.message,
        })),
      })
    )
    .pipe(gulpSass(dartSass).sync())
    .pipe(groupMedia())
    .pipe(autoprefixer({ cascade: false, grid: true }))
    .pipe($.gulp.dest($.paths.scss.public, { sourcemaps: true }))
    .pipe($.BrowserSync.stream());
};

export default scss;
