const paths = {
  views: {
    src: ["./src/views/*.html", "./src/views/pages/*.html"],
    public: "./public/",
    watch: [
      "./src/data/*.json",
      "./src/views/*.html",
      "./src/views/pages/**/*.html",
      "./src/views/blocks/**/*.html",
      "./src/views/components/**/*.html",
    ],
  },
  scripts: {
    src: "./src/js/**/*.js",
    public: "./public/js/",
    watch: ["./src/js/**/*.js"],
  },
  scss: {
    src: "./src/scss/main.scss",
    public: "./public/css/",
    watch: [
      "./src/scss/main.scss",
      "./src/scss/base/**/*.scss",
      "./src/scss/blocks/**/*.scss",
      "./src/scss/components/**/*.scss",
      "./src/scss/pages/**/*.scss",
    ],
  },
  fonts: {
    src: "./src/static/fonts/**/*.{eot,ttf,otf,woff,woff2}",
    public: "./public/fonts/",
    watch: "./src/static/fonts/**/*.{eot,ttf,otf,woff,woff2}",
  },
  images: {
    src: ["./src/static/img/**/*.{jpg,jpeg,png,tiff,svg}"],
    public: "./public/img/",
    watch: ["./src/static/img/**/*.{jpg,jpeg,png,tiff,svg}"],
  },
  favicons: {
    src: "./src/static/favicon/*.{jpg,jpeg,png,tiff,svg}",
    public: "./public/img/favicons/",
    watch: ["./src/static/favicon/*.{jpg,jpeg,png,tiff,svg}"],
  },
};

export { paths };
